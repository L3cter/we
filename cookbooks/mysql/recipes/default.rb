#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

rpm_package "mysql_repo" do
  source "http://dev.mysql.com/get/mysql57-community-release-el7-7.noarch.rpm"
end

template "mysql" do
  path "/etc/yum.repos.d/mysql-community.repo"
  source "mysql-community.repo.erb"
  owner "root"
  group "root"
  mode 0644
end

%w{mysql-community-server mysql-devel}.each do |pkg|
  package pkg do
    action :install
  end
end

service "mysqld" do
  action :start
  supports :status => true, :restart => true, :reload => true
end

bash "set root password" do
  code "mysqladmin -u root password '#{node["mysql"]["root_pass"]}'"
  only_if "mysql -u root -e 'show databases;'"
end

bash "create user" do
  user "root"
  group "root"
  code <<-EOC
    mysql -u root -p#{node["mysql"]["root_pass"]} -e "CREATE USER #{node["mysql"]["db_user"]}@localhost IDENTIFIED BY '#{node["mysql"]["user_pass"]}';"
    mysql -u root -p#{node["mysql"]["root_pass"]} -e 'GRANT ALL ON *.* TO #{node["mysql"]["db_user"]}@localhost;'
    mysql -u root -p#{node["mysql"]["root_pass"]} -e 'FLUSH PRIVILEGES;'
  EOC
  not_if "mysql -u root -p#{node["mysql"]["root_pass"]} -e \'show grants for '#{node["mysql"]["db_user"]}'@'localhost';\'"
end

template "database.yml" do
  path "/home/#{node["mysql"]["user"]}/workspace/database.yml"
  source "database.yml.erb"
  owner node["mysql"]["owner"]
  group node["mysql"]["group"]
end

__END__

bash "create user" do
  user "root"
  group "root"
  code <<-EOC
    mysql -u root -pnetwork -e "CREATE USER rails_user IDENTIFIED BY 'network';"
    mysql -u root -pnetwork -e 'GRANT ALL ON \`%development\`.* TO rails_user@localhost;'
    mysql -u root -pnetwork -e 'GRANT ALL ON \`%test\`.* TO rails_user@localhost;'
    mysql -u root -pnetwork -e 'GRANT ALL ON \`%production\`.* TO rails_user@localhost;'
    mysql -u root -pnetwork -e 'FLUSH PRIVILEGES;'
  EOC
  not_if "mysql -u root -pnetwork -e 'show grants for rails_user;'"
end
